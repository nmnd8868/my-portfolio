
# 경력 기술서

> - 최근 진행한 프로젝트 순으로 정리하였습니다.<br>
> - 일부 사이즈가 큰 이미지가 있어 로딩에 시간이 소요될 수 있습니다.<br>

***

## 아이오크롭스 (주)
* 재직기간 : 2019.01.10 ~ 현재
* 업무분야 : 자사 웹서비스 개발 (백앤드 / 프론트앤드)

*** 

### <신규개발 업무>
#### 아이오크롭스 클라우드 (https://cloud2.iocrops.com)
* 기간
  * 2019.01 ~ 현재
* 담당
  * 백앤드 / 프론트 앤드 전체 (기획 / UI디자인 제외)
* 소개
  * 데이터 기반 작물재배 지원 서비스로, 자사 센서 제품군과 타사 환경제어기로 부터 수집된 데이터를 분석 및 시각화하여 스마트팜(토마토, 파프리카, 딸기)농가의 의사결을 지원하는 서비스
* 구현
  * 기존 개발된 자사 센서 시스템과 농가에 설치된 다양한 환경제어기로 부터 센서 데이터 수집
  * 각 유저(데모 / 정품 사용자)별 실시간 데이터 제공 및 차트를 통한 시각화 서비스
    * 유저관리 / 노드 관리 / 수집 데이터 관리 등의 마이크로 서비스 형태로 구현
  * 수집 데이터를 분석(룰 기반)하여 관수에 대한 알림 기능 및 농업 의사결정에 필요한 각종 부가서비스 제공
* 기술스택
  * 백앤드
    * 개발언어 / 프레임워크 
      * Typescript, Node.js (Nest.js) / AWS Lambda (Serverless framework)
    * 서버 인프라 (AWS)
      * Cognito - 사용자 관리/인증 
      * IoT Core - Thing 관리, 센서데이터 구독 및 저장
      * Kinesis Firehorse - IoT Core로 부터 수집된 모든 데이터 캡쳐
      * ECS - Docker 컨테이너 기반의 앱 배포
      * 기타 - API Gateway, S3, Route53, Elastic Cache(Redis) 등
    * 데이터 베이스 
      * Maria DB (TypeORM 사용), AWS Dynamo DB
  * 프론트앤드
    * 개발언어 / 프레임워크 - ES6, React (상태관리 MobX)
    * Material UI / Styled components 사용
    * HighChart를 통한 데이터 시각화
    * Mobile UI / PWA형태의 개발로 모바일 / 데스크탑 설치 가능
    * 제플린을 통한 UI 디자이너와 협업
  * 동작화면 (※ 현재 실제 서비스 중이지만, 가입은 자제 부탁드립니다.)
  <center>
    <br>
    <img
      src="./ioCrops/iocropsDesktop.gif"
      width="640"
      height="350px"
      title="CSLNext" />
    <p>Desktop 화면</p>
  </center>
  
  <center>
    <img
      src="./ioCrops/iocropsMobile.gif"
      width="220"
      height="450px"
      title="CSLNext" />
    <p>Mobile 화면</p>
  </center>
***
## (주) SENTROL
* 재직기간 : 2016.12 ~ 현재
* 업무분야 : CNC / 3D프린터 관련 S/W 개발 (백앤드 / 프론트앤드)

***

### <유지보수 업무>
* 주물사 3D 프린터(SS150, SS600G) 프린팅 관리 S/W 유지보수 (C#, 윈폼)
* 오픈소스 슬라이싱 S/W, Slic3r 커스터마이징 및 유지보수 (C++)

***

### <신규개발 업무>
#### CSL NExt(Nextwork extension)
* 기간
  * 2017.10 ~ 2018.11
* 담당
  * 백앤드 / 프론트앤드 전체
* 소개
  * Linux기반으로 로컬 환경의 단일 기기 위에서 동작하는 CNC컨트롤러를 네트워크를 통해 원격에서 제어 하고 / 데이를 모니터링 하는 솔루션
* 구현
  * Linux 기반 CNC제어기의 데이터를 실시간으로 모니터링하고 시각화
  * 웹 상에서 복수개의 CNC제어기를 등록 하고 관리 할 수 있는 UI개발 (JWT 인증 / 계정별 MQTT 토픽관리 )
  * 웹 뿐만 아니라 다양한 클라이언트에서 원격으로 각 제어기를를 컨트롤 할 수 있는 API 개발
* 기술 스텍
  * 백앤드
    * 개발언어 / 프레임워크 - Typescript, C++ / Node.js (Express)
    * 데이터 베이스 - MySQL (TypeORM 사용)
  * 서버 인프라 (직접 구축)
    * 내부망에서 동작하는 자체 Linux(Ubuntu 16.04) 서버 구축 
    * Nginx, PM2, MySQL, Mosquitto(Mqtt broker server) 등
  * 프론트앤드 
    * 개발언어 / 프레임워크 - Typescript / React (MobX, Blueprint.js, SCSS, Styled-components 등)
    * 주요 기능 - 실시간 모니터링 데이터 출력 / 시각화 / 장비 제어 인터페이스 제공  
  * 동작화면 (※ 개발중 캡쳐한 이미지로 운용버전과 다소 차이가 있습니다.)
  
  <center>
    <br>
    <img
      src="./CSLNExt/CSLNextDesktop.gif"
      width="500"
      height="300px"
      title="CSLNext" />
    <p>Desktop 화면</p>
  </center>
  
  <center>
    <img
      src="./CSLNExt/CSLNextMobile.gif"
      width="210"
      height="400px"
      title="CSLNext" />
    <p>Mobile 화면</p>
  </center>

***

#### BinderJet 프린팅 S/W
* 기간
  * 2017.05 ~ 2017.11
* 담당
  * BinderJet 3D 프린터 헤드 제어 / 프린팅 관리 UI 개발
* 구현
  * BinderJet 방식의 3D프린터(SB300, SB400)의 바인더 헤드 및 제어기 축 제어
  * 3D 데이터로부터 슬라이싱된 대량의 이미지를 바인더 잉크헤드 규격의 포멧으로 변환 / 전송
  * 출력 현황 모니터링 / 바인더 분사량 조절 등 세부 설정 및 관리 기능
  * 재사용을 위한 모듈화 (DLL 형태의 라이브러리 구현)

* 기술 스텍
  * C# WPF / 소켓통신 / 시리얼 통신

* 결과물 (※ 개발중 캡쳐한 이미지로 운용버전과 차이가 있습니다.)
  
  <center>
    <img
      src="./Binderjet/Binderjet.gif"
      width="500"
      height="300px"
      title="Binderjet" />
    <p>Binderjet 테스트</p>
  </center>

***

#### Materialize Data Viewer
* 기간
  * 2017.02 ~ 2017.05
* 담당
  * 기능 / UI 개발 전체
* 구현
  * Materialize사의 전용 Binary 3D데이터 파싱 모듈 개발
  * 파싱된 데이터의 3D 결과물 출력 / 주요 속성 요약 기능
  * 재사용을 위한 모듈화 (DLL 형태의 라이브러리 구현)
* 기술 스텍
  * C# WPF, OpenGL 등
* 결과물 (※ 개발중 캡쳐한 이미지로 운용버전과 차이가 있습니다.)
  <center >
    <img
      src="./DataViewer/DataViewer.gif"
      width="500
      height="300px"
      title="Data Viewer">
    <p>Materialize Data Viewer</p>
  </center>
  
***
#### G-Code Viewer
* 기간
  * 2016.12 ~ 2017.2
* 담당
  * 기능 / UI 개발 전체
* 구현
  * CNC컨트롤러 제어를 위한 G-Code 파싱 / 2D 부분 형상화
  * 자사 컨트롤러에 특화된 G-Code 예약어, 커스텀 기능 지원
* 기술스텍
  * C# 윈폼, WPF Canvas 등
* 결과물
  
  <center >
    <img
      src="./GCodeViewer/GCodeViewer.gif"
      width="500
      height="300px"
      title="G-Code Viewer">
    <p>G-Code Viewer</p>
  </center>

***

## (주) PLATO 
* 재직기간 : 2015.08 ~ 2016.12
* 업무분야 : 자동차 스마트키 F/W, 응용 S/W 개발

***

### <유지보수 업무>
* 차량 블랙박스(옵니와칭) 영상 모니터링 S/W (자사 솔루션) 유지보수 및 버그픽스 (C++/MFC)

***
### <신규개발 업무>
#### Display FOB
* 기간
  * 2016.05 ~ 2016.12
* 담당
  * UI를 제외한 백그라운드 F/W / 기타 모니터링 소프트웨어
* 구현
  * LF인증 및 RF 통신 기능 (제공된 대형 차량 제어)
  * 이기종 마이크로칩(UI)과 I2C통신
  * 배터리 모니터링
* 기술 스택
  * FOB - MSP430 MCU, NCK2983 RF Tranceiver, MRF2678 LF Receiver
  * 모니터링/분석 툴 (C# WPF)
* 결과물 (UI 제외)
  * FOB 내부 기능 설정 및 로그 모니터링 S/W, 차량측 시뮬레이션 S/W
  * 동작 화면 (아래 이미지 참조)
  
  <center>
    <img
      src="./DisplayFOB/DisplayFOBAction.gif"
      width="200px"
      height="340px"
      title="Basestation">
    <p>Display Fob 동작</p>
  </center>

***

#### Low Frequency Basestation
* 기간
  * 2015.12 ~ 2016.06
* 담당
  * 해당 디바이스의 F/W 및 기타 모니터링 S/W 개발
* 구현
  * Fob 위치 판단을 위한 데이터 프레임 생성 및 출력하는 Basestation 개발
  * 스마트키 3대의 LF(Low Frequency) RSSI 신호를, 요구하는 형태의 RF(Radio Frequency)신호로 출력
  * Basestation 제어 S/W 및 모니터링, 플래쉬 메모리 라이팅 S/W 개발
* 기술스택
  * Basestation - STM8 MCU, TRF 4140/4260 LF Tranceiver, ATAs5781 RF
  * 모니터링/분석 툴 (C# WPF)
* 결과물
  * Basestation F/W
  * 로그 및 데이터 모니터링 S/W
  * RF칩셋 플래쉬 메모리 라이팅 S/W
  * 동작 화면 (시각화 할 수 없는 부분이 없습니다.)
